using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kick : MonoBehaviour
{
    const float MAX_HIT_FORCE = 3.5f;
    float hitForce = 3.5f;
    const float COOLDOWN = 0.2f;
    float curCooldown = 0;
    private BoxCollider kick;
    // Start is called before the first frame update
    private AudioSource kickSFX;
    float fullChargeupTime = 4;
    bool lmbHeld = false;
    [SerializeField]private AudioClip kickClip;
    [SerializeField]private AudioClip chargeClip;

    [SerializeField]private Transform angle;
    void Start()
    {
        kickSFX = GetComponent<AudioSource>();
        kick = GetComponent<BoxCollider>();
        kick.enabled = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetMouseButtonDown(0) && curCooldown <= 0) {
            kick.enabled = true;
            lmbHeld = true;
            //StartCoroutine("ChargeUp");

            //Attack anim
        }
        if (curCooldown > 0) {
            curCooldown -= Time.deltaTime;
            curCooldown = Mathf.Max(curCooldown, 0);
            kick.enabled = false;
        }
        if (Input.GetMouseButtonUp(0)) {
            lmbHeld = false;
            kick.enabled = false;
        }
    }

    IEnumerator ChargeUp() {
        kickSFX.clip = chargeClip;
        kickSFX.Play();
        float heldTime = 0;
        while (lmbHeld) {
            heldTime += Time.deltaTime;
            heldTime = Mathf.Min(heldTime, fullChargeupTime);
            hitForce = Mathf.Lerp(0, MAX_HIT_FORCE, heldTime/fullChargeupTime);
            yield return null;
        }
        kickSFX.Stop();
        curCooldown = COOLDOWN;
        kick.enabled = true;
    }

    void OnTriggerEnter(Collider c){
        if (kick.enabled) {
            Rigidbody rb = c.GetComponent<Rigidbody>();
            if (rb != null) {
                rb.AddForce(hitForce * angle.forward, ForceMode.Impulse);
                CleanPoop cleanPoop = c.GetComponent<CleanPoop>();
                if (cleanPoop != null) {
                    cleanPoop.CanDamage = true;
                }
                kickSFX.clip = kickClip;
                kickSFX.Play();
            }
        }
    }
}
