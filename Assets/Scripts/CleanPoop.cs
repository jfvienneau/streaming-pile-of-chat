using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CleanPoop : MonoBehaviour
{
    public bool canDamage = false;
    public bool CanDamage {
        get {return canDamage;}
        set {
            StopCoroutine("CheckStoppedMoving");
            StartCoroutine("CheckStoppedMoving");
            canDamage = true;
        }
    }
    Rigidbody rb;
    private float velocityToDmg = 0.2f;
    AudioSource hitSource;
    WinManager winManage;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        hitSource = GetComponent<AudioSource>();
        winManage = GameObject.Find("WinCondition").GetComponent<WinManager>();
    }


    IEnumerator CheckStoppedMoving() {
        yield return new WaitForSeconds(1f);
        while(canDamage) {
            if(rb.velocity.sqrMagnitude < .01f && rb.angularVelocity.sqrMagnitude < .01f) {
                yield return new WaitForSeconds(0.2f);
                canDamage = false;
            }
            yield return new WaitForFixedUpdate();
        }
    }

    void OnCollisionEnter(Collision c) {
        if (canDamage) {
            ChatBehavior chatBehave = c.gameObject.GetComponent<ChatBehavior>();
            if (chatBehave != null && chatBehave.CurBehavior != behavior.stunned) {
                chatBehave.CurBehavior = behavior.stunned;
                winManage.StreamerPts += 5;
                hitSource.Play();
            }
        }
    }
}
