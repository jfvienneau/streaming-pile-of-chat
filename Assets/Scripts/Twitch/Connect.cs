
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

    public struct MessageInfo {
        public string user;
        public string channel;
        public string fullMessage;
        public string hexColor;
        public string action;
    }
public class Connect : MonoBehaviour
{
    [SerializeField] private TwitchConnect twitchConnect;
    [SerializeField] private InputField channelNameBox;
    [SerializeField] private SpawnChat spawner;

    private void Awake()
    {
        Application.targetFrameRate = 30;
        Application.runInBackground = true;
    }

    private void Update()
    {
        if (twitchConnect.NewTwitchMessage(out string newMessage))
        {
            print(MessageFormate(newMessage));
            string returnMsg = spawner.ManageMessage(MessageFormate(newMessage));
            if (returnMsg.Length > 0) {
                SendMessage(returnMsg);
            }
        }
    }

    public void ConnectToTwitch()
    {
        twitchConnect.ConnectToTwitch();
    }
    public void JoinChannel()
    {
        twitchConnect.JoinChannel(channelNameBox.text);
    }
    public void LeaveChannel()
    {
        twitchConnect.LeaveChannel(channelNameBox.text);
    }
    public void SendMessage(string message)
    {
        twitchConnect.WriteToChannel(channelNameBox.text, message);
    }

    private MessageInfo MessageFormate(string twitchLine)
    {
        MessageInfo msg = twitchConnect.messageDetails(twitchLine);
        if (msg.user != null)
        {
            msg.action = msg.fullMessage.Split(' ')[0];
        }

        return msg;
    }

    public void StartGame() {
        spawner.gameStarted = true;
        GameObject.Find("Player").GetComponent<StreamerMovement>().movementSpeed = StreamerMovement.DEFAULT_MOVEMENT_SPEED;
        GameObject.Find("BGMusic").GetComponent<AudioSource>().Play();
        GameObject.Find("WaitingBGMusic").GetComponent<AudioSource>().Stop();
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void StopGame() {
        spawner.gameStarted = false;
        GameObject.Find("Player").GetComponent<StreamerMovement>().movementSpeed = 0;
        StartCoroutine(Connect.StartFade(GameObject.Find("BGMusic").GetComponent<AudioSource>(), 5f, 0));
        GameObject.Find("WinningMusic").GetComponent<AudioSource>().Play();
        Cursor.lockState = CursorLockMode.None;
    }

    public static IEnumerator StartFade(AudioSource audioSource, float duration, float targetVolume)
    {
        float currentTime = 0;
        float start = audioSource.volume;

        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            audioSource.volume = Mathf.Lerp(start, targetVolume, currentTime / duration);
            yield return null;
        }
        yield break;
    }
}