using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu]
public class TwitchConnect : ScriptableObject
{
    public string accountName;
    public string Token;

    private TcpClient tcpClient;
    private StreamReader streamReader;
    private StreamWriter streamWriter;

    public async void ConnectToTwitch()
    {
        tcpClient = new TcpClient();

        await tcpClient.ConnectAsync("irc.chat.twitch.tv", 6667);

        streamReader = new StreamReader(tcpClient.GetStream());
        streamWriter= new StreamWriter(tcpClient.GetStream()) { NewLine = "\r\n", AutoFlush = true };


        await streamWriter.WriteLineAsync("CAP REQ :twitch.tv/tags ");
        await streamWriter.WriteLineAsync("PASS " + Token);
        await streamWriter.WriteLineAsync("NICK " + Token);

        ReadMessages();

    }


    public async void WriteToChannel(string channelName, string messageToSend)
    {
        await streamWriter.WriteLineAsync($"PRIVMSG #{channelName} :{messageToSend}");
    }

    public async void JoinChannel(string channelName)
    {
        await streamWriter.WriteLineAsync("JOIN #" + channelName);
    }
    public async void LeaveChannel(string channelName)
    {
        await streamWriter.WriteLineAsync("PART #" + channelName);
    }

    private string lastLine;
    [SerializeField] private List<string> logs = new List<string>();
    [SerializeField] private int logsIndex;
    [SerializeField] private bool isClearLogs = false;
    private async void ReadMessages()
    {
        logs.Clear();
        logsIndex = 1;
        while (true)
        {
            if (isClearLogs)
            {
                logs.Clear();
                logsIndex = 1;
                isClearLogs = false;

                logs.Add(lastLine);
            }

            lastLine = await streamReader.ReadLineAsync();
            logs.Add(lastLine);

            Debug.Log(lastLine);
            if (lastLine.Contains(" JOIN ")) {
                GameObject.Find("Start").GetComponent<Button>().interactable = true;
            }

            if (lastLine != null && lastLine.StartsWith("PING"))
            {
                lastLine.Replace("PING", "PONG");
                await streamWriter.WriteLineAsync(lastLine);
            }

        }
    }
    public void ClearLogs()
    {
        isClearLogs = true;
    }
    public bool NewTwitchMessage(out string newMessage)
    {
        if (logs.Count < logsIndex | logs.Count==0)
        {
            newMessage = "";
            return false;
        }

        for (int i = logsIndex; i <= logs.Count; i++)
        {
            if (logs[i - 1].Contains("PRIVMSG"))
            {
                logsIndex = i + 1;
                newMessage = logs[i - 1];
                return true;
            }
        }

        logsIndex = logs.Count + 1;
        newMessage = "";
        return false;

    }

    public MessageInfo messageDetails(string twitchMessage)
    {
        MessageInfo info = new MessageInfo();
        if (twitchMessage == null)
        {
            return info;
        }
        if (!twitchMessage.Contains("PRIVMSG"))
        {
            return info;
        }

        string[] tags = twitchMessage.Split(' ')[0].Split(';');

        info.user = Array.Find(tags, tag => {return tag.Split('=')[0] == "display-name";}).Split('=')[1];
        info.hexColor = Array.Find(tags, tag => {return tag.Split('=')[0] == "color";}).Split('=')[1];

        string[] firstSplit = twitchMessage.Split(' ');

        info.channel = firstSplit[3].Substring(1);

        string[] messageSplit = twitchMessage.Split(':');
        info.fullMessage = messageSplit[2];
        return info;
    }

}