using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WinManager : MonoBehaviour
{
    [SerializeField]private TMP_Text chatMarks;
    [SerializeField]private TMP_Text streamerPoints;
    [SerializeField]private TMP_Text winText;
    [SerializeField]private GameObject winUI;

    [SerializeField]private int streamGoal = 50;
    private int chatPts = 0;
    private int chatGoal = 20;
    public int ChatPts { get{return chatPts;}
        set{
            chatPts = Mathf.Max(value,0);
            chatMarks.text = string.Format("Chat's Marks {0}/{1}",chatPts,chatGoal);
            CheckChatWinCond();
        }
    }

    public int ChatGoal { get{return chatGoal;}
        set{
            chatGoal = Mathf.Max(value,0);
            chatMarks.text = string.Format("Chat's Marks {0}/{1}",chatPts,chatGoal);
        }
    }

    private int streamerPts = 0;
    public int StreamerPts { get{return streamerPts;}
        set{
            streamerPts = Mathf.Max(value,0);
            streamerPoints.text = string.Format("Streamer Points {0}/{1}", streamerPts, streamGoal);
            CheckStreamerWinCond();
        }
    }

    void Start()
    {
        ChatPts = 0;
        StreamerPts = 0;
    }

    void CheckChatWinCond() {
        if (chatPts >= chatGoal) {
            winText.text = "Chat Won!";
            ShowWin();
        }
    }

    void CheckStreamerWinCond() {
        if (streamerPts >= streamGoal) {
            winText.text = "Streamer Won!";
            ShowWin();
        }
    }

    void ShowWin() {
        winUI.SetActive(true);
        GameObject.Find("TwtichConnect").GetComponent<Connect>().StopGame();
    }
}
