using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum behavior {
    taunt = 0,
    idle,
    flee,
    attack,
    jump,
    teleport,
    fart,
    stunned
}
public class ChatBehavior : MonoBehaviour
{

    private GameObject player;
    [SerializeField]private float jumpCooldown = 18f;
    [SerializeField]private Rigidbody rb;
    [SerializeField]private float fleeForce = 10f;
    [SerializeField]private float jumpForce = 15f;

    [SerializeField]private float attackForce = 10f;
    private bool canPlaceStain = false;
    private bool canTeleport = true;

    [SerializeField]private float tauntCooldown = 15f;
    [SerializeField]private AudioClip[] tauntSFXs;
    [SerializeField]private GameObject stain;

    [SerializeField]private GameObject fart;
    [SerializeField]private float fartCooldown = 35f;

    private Dictionary<behavior,float> cooldown = new Dictionary<behavior,float>();
    private behavior curBehavior = behavior.idle;

    [SerializeField]private float stunnedCooldown = 15f;

    [SerializeField]private MeshRenderer poopRender;
    [SerializeField]private Material activeMaterial;
    [SerializeField]private Material stunnedMaterial;

    private AudioSource audioS;
    private WinManager winManager;
    [SerializeField]private AudioClip[] hurtSFXs;

    public behavior CurBehavior {
        get {return curBehavior;}
        set {
            if (curBehavior != behavior.stunned) {
                curBehavior = value;
            }

        }
    }

    void Start()
    {
        player = GameObject.Find("Player");
        audioS = GetComponent<AudioSource>();
        winManager = GameObject.Find("WinCondition").GetComponent<WinManager>();
        foreach(behavior key in behavior.GetValues(typeof(behavior))) {
            cooldown[key] = 0;
        }
        StartCoroutine("HandleFSM");
    }

    IEnumerator HandleFSM()
    {
        while (true) {
            switch(curBehavior) {
                case behavior.jump:
                    if (cooldown[curBehavior] <= 0) {
                        Jump();
                        cooldown[curBehavior] = jumpCooldown;
                    }
                break;
                case behavior.taunt:
                    if (cooldown[curBehavior] <= 0) {
                        Taunt();
                        cooldown[curBehavior] = tauntCooldown;
                    }
                break;
                case behavior.teleport:
                    if (canTeleport) {
                        transform.position = SpawnChat.GetRandomGameBoardLocation();
                        canTeleport = false;
                    }
                    curBehavior = behavior.jump;
                break;
                case behavior.fart:
                    if (cooldown[curBehavior] <= 0) {
                        Fart();
                        cooldown[curBehavior] = fartCooldown;
                    }
                    curBehavior = behavior.jump;
                break;
                case behavior.flee:
                    Vector3 awayFromPlayer = (transform.position - player.transform.position);
                    rb.AddForce(awayFromPlayer.normalized * fleeForce, ForceMode.Force);
                break;
                case behavior.attack:
                    Vector3 towardsPlayer = (transform.position - player.transform.position) * -1;
                    rb.AddForce(towardsPlayer.normalized * attackForce, ForceMode.Force);
                break;
                case behavior.stunned:
                    if (cooldown[curBehavior] <= 0) {
                        cooldown[curBehavior] = stunnedCooldown;
                        StartCoroutine("Stunned");
                    }
                break;
            }

            foreach(behavior key in behavior.GetValues(typeof(behavior))) {
                cooldown[key] -= Time.deltaTime;
                cooldown[key] = Mathf.Max(cooldown[key], 0);
            }
            yield return null;
        }
    }

    void Taunt() {
        audioS.clip = tauntSFXs[Random.Range(0,tauntSFXs.Length-1)];
        audioS.Play();
        winManager.StreamerPts -= 3;
    }

    void Jump() {
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, Random.Range(0, 360), transform.eulerAngles.z);
        rb.isKinematic = false;
        Vector3 force = transform.forward;
        force = new Vector3(force .x, 1, force .z);
        rb.AddForce(force * jumpForce, ForceMode.Impulse);
        canPlaceStain = true;
    }
    private float maxVelocity = 20;
    void FixedUpdate() {
     rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxVelocity);
    }

    void Fart() {
        GameObject fartGO = Instantiate(fart, transform.position, Quaternion.identity);
        Destroy(fartGO, 24f);
    }

    IEnumerator Stunned() {
        poopRender.material = stunnedMaterial;
        audioS.clip = hurtSFXs[Random.Range(0,hurtSFXs.Length-1)];
        audioS.Play();
        while(cooldown[behavior.stunned] > 0) {
            yield return null;
        }
        poopRender.material = activeMaterial;
        curBehavior = behavior.teleport;
    }

    void OnCollisionEnter(Collision c) {
        if (canPlaceStain && c.gameObject.layer == LayerMask.NameToLayer("SpawnableGround")) {
            Vector3 hitLocation = c.contacts[0].point;
            winManager.ChatPts++;
            Instantiate(stain, new Vector3(hitLocation.x, -11, hitLocation.z), Quaternion.Euler(90, 0, 0));
            canPlaceStain = false;
        }
        if (c.gameObject.tag == "Player" && curBehavior != behavior.stunned) {
            StreamerMovement movement = c.gameObject.GetComponent<StreamerMovement>();
            if (!movement.isDebuff) {
                movement.MovementSpeedDebuff = 4f;
            }

        }
    }
}
