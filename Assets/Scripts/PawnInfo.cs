using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PawnInfo : MonoBehaviour
{
    private string username;

    private Camera cam;
    [SerializeField]private TMP_Text msg;
    public string Username {
        get {return username; }
        set {
            msg.text = value;
            username = value;
        }
    }
    private Color color;

    private Color fontColor;
    public string FontColor {
        get {return fontColor.ToString(); }
        set {

            ColorUtility.TryParseHtmlString(value, out color);
            msg.color = color;
            fontColor = msg.color;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        cam = (Camera) FindObjectOfType(typeof(Camera));
        msg.canvas.worldCamera = cam;

        fontColor = new Color(255, 255, 255);
    }

    // Update is called once per frame
    void Update()
    {
        msg.transform.LookAt(cam.transform);
        msg.transform.rotation = Quaternion.LookRotation(cam.transform.forward);
    }
}
