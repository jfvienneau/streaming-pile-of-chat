using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StreamerMovement : MonoBehaviour
{
    public const float DEFAULT_ROTATE_SPEED = 100;
    public const float SPRINT_ROTATE_SPEED = 50;

    public const float DEFAULT_MOVEMENT_SPEED = 6f;
    public const float SPRINT_MOVEMENT_SPEED = 12f;
    public float movementSpeed = DEFAULT_MOVEMENT_SPEED;
    float rotateSpeed = DEFAULT_ROTATE_SPEED;

    [SerializeField]private Rigidbody controls;
    [SerializeField]private AudioSource playerSounds;
    [SerializeField]private AudioClip[] coughs;

    private float debuffEffectTime = 4f;
    public bool isDebuff = false;
    public float MovementSpeedDebuff {
        get {return movementSpeed;}
        set {
            //Restart process
            StopCoroutine("DebuffTimer");
            StartCoroutine("DebuffTimer");
            playerSounds.clip = coughs[Random.Range(0, coughs.Length-1)];
            playerSounds.Play();
            isDebuff = true;
            movementSpeed = value;
        }
    }

    IEnumerator DebuffTimer()
    {
        float ellapseTime = 0;
        while (ellapseTime < debuffEffectTime) {
            ellapseTime += Time.deltaTime;
            yield return null;
        }
        movementSpeed = DEFAULT_MOVEMENT_SPEED;
        isDebuff = false;
    }


    void FixedUpdate()
    {
        if (!isDebuff && movementSpeed > 0) {
            if (Input.GetKeyDown(KeyCode.LeftShift)) {
                movementSpeed = SPRINT_MOVEMENT_SPEED;
                rotateSpeed = SPRINT_ROTATE_SPEED;
            } else if (Input.GetKeyUp(KeyCode.LeftShift)) {
                movementSpeed = DEFAULT_MOVEMENT_SPEED;
                rotateSpeed = DEFAULT_ROTATE_SPEED;
            }
        }


        //Store user input as a movement vector
        Vector3 m_Input = controls.rotation * new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        //Apply the movement vector to the current position, which is
        //multiplied by deltaTime and speed for a smooth MovePosition
        controls.MovePosition(transform.position + m_Input * Time.deltaTime * movementSpeed);

        //Set the angular velocity of the Rigidbody (rotating around the Y axis, 100 deg/sec)
        Vector3 m_EulerAngleVelocity = new Vector3(0, rotateSpeed, 0);
        Quaternion deltaRotation = Quaternion.Euler(m_EulerAngleVelocity * Mathf.Clamp(Input.GetAxis("Mouse X"), -4, 4) * Time.fixedDeltaTime);
        controls.MoveRotation(controls.rotation * deltaRotation);

    }
}

    // Update is called once per frame
        /*float mouse = Input.GetAxis("Horizontal");
        controls.AddForce(transform.forward * Input.GetAxis("Vertical") * movementSpeed);
        transform.Rotate(new Vector3(0f, mouse * rotateSpeed, 0f));

        */
