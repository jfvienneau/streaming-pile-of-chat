using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class SpawnChat : MonoBehaviour
{
    Dictionary<string, ChatBehavior> chatPawns = new Dictionary<string, ChatBehavior>();
    [SerializeField]private WinManager win;
    public bool gameStarted = false;
        public string ManageMessage(MessageInfo info) {
        string outcomeMsg = "";
        switch(info.action.ToLower()) {
            case "poop":
            case "shit":
            case "caca":
            case "dump":
            case "crap":
            case "shite":
            case "deuce":
            case "stool":
            case "poo":
            case "feces":
                outcomeMsg = AddPawn(info);
            break;
            case "fart":
                PawnAction(info.user, behavior.fart);
            break;
            case "retreat":
            case "flee":
                PawnAction(info.user, behavior.flee);
            break;
            case "attack":
            case "engage":
                PawnAction(info.user, behavior.attack);
            break;
            case "stay":
            case "stop":
            case "pause":
            case "wait":
                PawnAction(info.user, behavior.idle);
            break;
            case "taunt":
            case "showoff":
            case "insult":
                PawnAction(info.user, behavior.taunt);
            break;
            case "jump":
            case "hop":
            case "leap":
                PawnAction(info.user, behavior.jump);
            break;
            case "teleport":
                PawnAction(info.user, behavior.teleport);
            break;
        }

        return outcomeMsg;
    }
    // Start is called before the first frame update
    public string AddPawn(MessageInfo info)
    {
        string username = info.user;
        string returnMessage = string.Format("@{0} ", username);
        if (!chatPawns.ContainsKey(username)) {

            GameObject newPawn = Instantiate(Resources.Load("Prefabs/ChatPawn"), GetRandomGameBoardLocation(), new Quaternion(0,0,0,0)) as GameObject;
            newPawn.transform.parent = transform;
            PawnInfo userInfo = newPawn.GetComponent<PawnInfo>();
            userInfo.Username = username;
            userInfo.FontColor = info.hexColor;
            chatPawns.Add(username, newPawn.GetComponent<ChatBehavior>());
            win.ChatGoal += 10;
            returnMessage += "New Poop!";
        } else {
            //Pawn already exists
            returnMessage += "Poop already pooping";
        }
        return returnMessage;
    }

    public string PawnAction(string username, behavior action) {
        string returnMessage = string.Format("@{0} ", username);

        if (!gameStarted) {
            returnMessage += "Hold ur horses, not started yet";
        } else if (chatPawns.ContainsKey(username)) {
            chatPawns[username].CurBehavior = action;
            returnMessage = ""; //Dont need to say something
        } else {
            returnMessage += "404 Poop not found";
        }

        return returnMessage;
    }

    public static Vector3 GetRandomGameBoardLocation()
    {
        return new Vector3(Random.Range(-48,48),-10,Random.Range(-48,48));
    }
}
