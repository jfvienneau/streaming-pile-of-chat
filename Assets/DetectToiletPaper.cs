using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectToiletPaper : MonoBehaviour
{
    private AudioSource AudioS;
    [SerializeField]private AudioClip[] splats;
    void Start() {
        AudioS = GetComponent<AudioSource>();
        AudioS.clip = splats[Random.Range(0, splats.Length-1)];
        AudioS.Play();
    }
        void OnTriggerEnter(Collider c) {
        if (c.gameObject.tag == "ToiletPaper") {
            GameObject.Find("WinCondition").GetComponent<WinManager>().ChatPts--;
            Destroy(gameObject);
        }
    }
}
